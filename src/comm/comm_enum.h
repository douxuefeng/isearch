/*
 * =====================================================================================
 *
 *       Filename:  comm_enum.h
 *
 *    Description:  common enum definition.
 *
 *        Version:  1.0
 *        Created:  26/08/2021
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  zhulin, shzhulin3@jd.com
 *        Company:  JD.com, Inc.
 *
 * =====================================================================================
 */

#ifndef __COMM_ENUM_H__
#define __COMM_ENUM_H__
#include <string>
#include <map>

enum SORTTYPE {
    SORT_RELEVANCE = 1, // 默认，按相关性排序
    DONT_SORT = 3, //不排序
    SORT_FIELD_ASC = 4, // 按字段升序
    SORT_FIELD_DESC = 5, // 按字段降序
    SORT_GEO_DISTANCE = 6 // 按距离升序
};

enum FieldType{
    FIELD_INT = 1,
    FIELD_STRING = 2,
    FIELD_TEXT = 3,
    FIELD_IP = 4,
    FIELD_GEO_POINT = 5,
    FIELD_LAT = 6,
    FIELD_GIS = 7,
    FIELD_DISTANCE = 8,
    FIELD_DOUBLE = 9,
    FIELD_LONG = 10,
    FIELD_INDEX = 11,
    FIELD_LNG_ARRAY = 12,
    FIELD_LAT_ARRAY = 13,
    FIELD_GEO_SHAPE = 14,
    FIELD_VECTOR = 15,
};

class FieldTypeMap{
public:
    static void Init(){
        field_type_map_.insert(std::make_pair("int", FIELD_INT));
        field_type_map_.insert(std::make_pair("string", FIELD_STRING));
        field_type_map_.insert(std::make_pair("text", FIELD_TEXT));
        field_type_map_.insert(std::make_pair("ip", FIELD_IP));
        field_type_map_.insert(std::make_pair("geo_point", FIELD_GEO_POINT));
        field_type_map_.insert(std::make_pair("double", FIELD_DOUBLE));
        field_type_map_.insert(std::make_pair("long", FIELD_LONG));
        field_type_map_.insert(std::make_pair("index", FIELD_INDEX));
        field_type_map_.insert(std::make_pair("geo_shape", FIELD_GEO_SHAPE));
        field_type_map_.insert(std::make_pair("vector", FIELD_VECTOR));
    }
    static int GetIntType(std::string str){
        if(field_type_map_.find(str) == field_type_map_.end()){
            return 0;
        }
        return field_type_map_.at(str);
    }
private:
    static std::map<std::string, int> field_type_map_;
};

enum SEGMENTTAG {
    SEGMENT_NONE = 0,
    SEGMENT_DEFAULT = 1,
    SEGMENT_NGRAM = 2,
    SEGMENT_CHINESE = 3,
    SEGMENT_ENGLISH = 4,
    SEGMENT_RANGE = 5,
};

enum VectorRetCode{
    RT_SUCCESS = 0,
    RT_PARAMETER_ERROR = 30001,
    RT_ROCKSDB_ADD_ERROR,
    RT_ROCKSDB_QUERY_ERROR,
    RT_FAISS_SEARCH_ERROR,
    RT_FAISS_ADD_ERROR,
    RT_NO_MEMORY,
    RT_RECV_VECTOR_ERROR,
    RT_PARSE_ARRAY_ERROR,
};

#endif