/*
 * =====================================================================================
 *
 *       Filename:  vector_processor.h
 *
 *    Description:  VectorProcessor class definition.
 *
 *        Version:  1.0
 *        Created:  26/10/2021 18:02:05 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  shzhulin3, shzhulin3@jd.com
 *        Company:  JD.com, Inc.
 *
 * =====================================================================================
 */

#ifndef __VECTOR_PROCESSOR_H__
#define __VECTOR_PROCESSOR_H__

#include "vector_uds_client.h"
#include "singleton.h"
#include <string>

#define BUFFSIZE 10240

class VectorRsp;
class VectorProcessor{
public:
    VectorProcessor();
    ~VectorProcessor();
    static VectorProcessor *Instance(){
        return CSingleton<VectorProcessor>::Instance();
    }

    static void Destroy(){
        CSingleton<VectorProcessor>::Destroy();
    }
    bool InitVectorService(std::string vector_socket_dir);
    int SendToVectorService(char* buffer, int buffer_len, int cmd_type, VectorRsp& vector_add_rsp);

private:
    VecUDSClient* uds_client_;
};

#endif