#ifndef VECTOR_DATA_GENERATOR_H_
#define VECTOR_DATA_GENERATOR_H_

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netinet/in.h>
#include <netdb.h>  /* netdb is necessary for struct hostent */
#include <fstream>
#include <sstream>
#include <iostream>
#include <random>
#include "json/json.h"

struct ArgvCnt{
    std::string s_program_dir;
    int ui_dim;
    int ui_vet_num;
    int ui_appid;
    int ui_fieldid;
    int ui_index_type;
    std::string s_save_filename;
};

int main(int argc, char *argv[])
{
    if (argc != 7) {
        printf("Usage: %d <dim> <vet_num> <appid> <fieldid> <index_type> <save_filename>\n", argc);
        exit(1);
    }

    ArgvCnt o_argv_cnt;
    o_argv_cnt.s_program_dir = argv[0];
    o_argv_cnt.ui_dim = atoi(argv[1]);
    o_argv_cnt.ui_vet_num = atoi(argv[2]);
    o_argv_cnt.ui_appid = atoi(argv[3]);
    o_argv_cnt.ui_fieldid = atoi(argv[4]);
    o_argv_cnt.ui_index_type = atoi(argv[5]);
    o_argv_cnt.s_save_filename = argv[6];

    std::mt19937 rng;
    std::uniform_real_distribution<> distrib;
    Json::Value o_send_data;
    o_send_data["appId"] = Json::Value(o_argv_cnt.ui_appid);
    o_send_data["fieldId"] = Json::Value(o_argv_cnt.ui_fieldid);
    o_send_data["defaultIndexType"] = Json::Value(o_argv_cnt.ui_index_type);

    std::ofstream os;
    os.open(o_argv_cnt.s_save_filename, std::ios::out | std::ios::app);
    if (!os.is_open()) {
        std::cout << "error：can not find or create the file which named: " << o_argv_cnt.s_save_filename << std::endl;
    }

    Json::FastWriter sw;
    for (int i = 0; i < o_argv_cnt.ui_vet_num; i++) {
        for (int j = 0; j < o_argv_cnt.ui_dim; j++) {
            o_send_data["vectorData"].append(distrib(rng));
        }
        os << sw.write(o_send_data);
        o_send_data["vectorData"].clear();
    }
    os.close();
    return 0;
};
#endif