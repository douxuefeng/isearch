#ifndef VET_PROTOBUF_CONVERTOR_H_
#define VET_PROTOBUF_CONVERTOR_H_

/**
 * pb head files all convert here
*/
#include "vet_index_common.h"
#include "protoc/vector.pb.h"

VET_INDEX_NAMESPACE_BEGIN

class PbCvtor
{
public:
    PbCvtor() {};
    ~PbCvtor() {};

public:
    static bool ConvertPbStr2VetReqCnt(const std::string& s_buf , VetReqCnt& o_vetreq);
    
    static bool ConvertVetRspCnt2PbStr(const VetRspCnt& o_vet_rsp , std::string& s_buf);
};

VET_INDEX_NAMESPACE_END
#endif