#ifndef CONFIG_MANAGER_H_
#define CONFIG_MANAGER_H_

#include "vet_index_common.h"
#include "singleton.h"
#include "noncopyable.h"
#include "config_app_field_def_parser.h"
#include "config_vector_index_parser.h"

VET_INDEX_NAMESPACE_BEGIN

enum CONFIG_FILE_DEFINE {
    E_CONFIG_FILE_APP_FIELD,
    E_CONFIG_FILE_VECTOR_INDEX,
    E_CONFIG_FILE_END_NUM
} ;
using ConfigFileType = CONFIG_FILE_DEFINE;

class ConfigManager : private noncopyable
{
public:
    ConfigManager();
    virtual ~ConfigManager();

public:
    static ConfigManager* Instance()
    {
        return CSingleton<ConfigManager>::Instance();
    };

    static void Destroy()
    {
        CSingleton<ConfigManager>::Destroy();
    };

public:
    bool InitConfig();

    const AppFieldContext* const GetAppFieldTable() {
        AppFieldContext* p_app_field_cont = 
            dynamic_cast<AppFieldContext*>(
                o_config_parser_map_[E_CONFIG_FILE_APP_FIELD]);
        
        assert(p_app_field_cont != NULL);
        return p_app_field_cont;
    };

    const VectorIndexContext* const GetVectorIndexTable() {
         VectorIndexContext* p_vet_index_cont = 
            dynamic_cast<VectorIndexContext*>(
                o_config_parser_map_[E_CONFIG_FILE_VECTOR_INDEX]);

        assert(p_vet_index_cont != NULL);
        return p_vet_index_cont;
    };
private:
    void register_config_parser();

private:
    std::map<ConfigFileType , ConfigParserBase*> o_config_parser_map_;
};

VET_INDEX_NAMESPACE_END
#endif