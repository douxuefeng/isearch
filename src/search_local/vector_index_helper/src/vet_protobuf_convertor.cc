#include "vet_protobuf_convertor.h"

VET_INDEX_NAMESPACE_BEGIN

bool PbCvtor::ConvertPbStr2VetReqCnt(
        const std::string& s_buf ,
        VetReqCnt& o_vetreq)
{
    VectorReq o_vet_req;
    if (!o_vet_req.ParseFromString(s_buf)) {
        return false;
    }
    
    for (int i = 0; i < o_vet_req.vector_data().size(); i++) {
        o_vetreq.o_vet_data_.emplace_back(o_vet_req.vector_data(i));
    }
    
    if (o_vet_req.has_topk()) {
        o_vetreq.ui_top_k_ = o_vet_req.topk();
    } else {
        o_vetreq.ui_top_k_ = 10;
    }
    
    o_vetreq.o_idx_flag_.i_appid_ = o_vet_req.appid();
    o_vetreq.o_idx_flag_.i_field_id_ = o_vet_req.field_id();

    if (o_vet_req.has_index_typeid()) {
        o_vetreq.o_idx_flag_.b_has_index_typeid_ = true;
        o_vetreq.o_idx_flag_.i_index_typeid_ = o_vet_req.index_typeid();
    } else {
        o_vetreq.o_idx_flag_.b_has_index_typeid_ = false;
    }
    
    return true;
}

bool PbCvtor::ConvertVetRspCnt2PbStr(
    const VetRspCnt& o_vet_rsp ,
    std::string& s_buf)
{
    VectorRsp o_vet_rsp_pb;
    for (size_t i = 0; i < o_vet_rsp.vet_ids.size(); i++) {
        o_vet_rsp_pb.add_vector_id(o_vet_rsp.vet_ids[i]);
    }

    for (size_t j = 0; j < o_vet_rsp.vet_dis.size(); j++) {
        o_vet_rsp_pb.add_vector_dis(o_vet_rsp.vet_dis[j]);
    }

    o_vet_rsp_pb.set_code(o_vet_rsp.ui_code);
    o_vet_rsp_pb.set_info(o_vet_rsp.s_info);

    return o_vet_rsp_pb.SerializeToString(&s_buf);
}

VET_INDEX_NAMESPACE_END
