#include "faiss_index_ivf_flat.h"

#include "invlists/DirectMap.h"
#include "log.h"

VET_INDEX_NAMESPACE_BEGIN

IndexIVFFlat::IndexIVFFlat(faiss::IndexIVFFlat* p_index)
    : FaissAptBase(p_index)
    , p_index_ivf_flat_(p_index)
{
    p_index_ivf_flat_->set_direct_map_type(faiss::DirectMap::Hashtable);
}

IndexIVFFlat::~IndexIVFFlat()
{

}

int IndexIVFFlat::UpdateVet(
    const float* p_vetcont_array,
    const idx_t* p_vetid_array,
    idx_t i_num)
{
    try
    {
        p_index_ivf_flat_->update_vectors(
            i_num,
            p_vetid_array,
            p_vetcont_array);
    }
    catch(const faiss::FaissException& e)
    {
        log_error("update vector error:%s" , e.what());
        return -1;
    }
    catch(...)
    {
        log_error("unknow error");
        return -1;
    }

    return 0;
}

/***************************************************/

IndexIVFPQ::IndexIVFPQ(faiss::IndexIVFPQ* p_index)
    : FaissAptBase(p_index)
    , p_index_ivf_pq_(p_index)
{
    p_index_ivf_pq_->set_direct_map_type(faiss::DirectMap::Hashtable);
}

IndexIVFPQ::~IndexIVFPQ()
{

}

int IndexIVFPQ::UpdateVet(
    const float* p_vetcont_array,
    const idx_t* p_vetid_array,
    idx_t i_num)
{
    try
    {
        p_index_ivf_pq_->update_vectors(
            i_num,
            p_vetid_array,
            p_vetcont_array);
    }
    catch(const faiss::FaissException& e)
    {
        log_error("update vector error:%s" , e.what());
        return -1;
    }
    catch(...)
    {
        log_error("unknow error");
        return -1;
    }

    return 0;
}

VET_INDEX_NAMESPACE_END