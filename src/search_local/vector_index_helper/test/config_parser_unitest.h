#ifndef CONFIG_PARSER_UNITEST_H_
#define CONFIG_PARSER_UNITEST_H_

#include "unitest_comm.h"
#include "../include/config_parser/config_manager.h"

UNITEST_NAMESPACE_BEGIN

class ConfigParserTest : public testing::Test{
protected:
    virtual void SetUp() {
        bool b_ret = o_app_field_cnt.ParseConfig();
        EXPECT_TRUE(b_ret);

        b_ret = o_vector_idx_cnt.ParseConfig();
        EXPECT_TRUE(b_ret);
    }

    vectorindex::AppFieldContext o_app_field_cnt;
    vectorindex::VectorIndexContext o_vector_idx_cnt;
    vectorindex::ConfigManager* p_config_manager;
};

TEST_F(ConfigParserTest, AppFieldCntTest){
    EXPECT_TRUE(o_app_field_cnt.o_app_field_table_.size() == 1);

    int i_appid = 10008;
    int i_field_id = 14;
    EXPECT_TRUE(o_app_field_cnt.o_app_field_table_[i_appid][i_field_id].ui_dim_ == 128);
    EXPECT_TRUE(o_app_field_cnt.o_app_field_table_[i_appid][i_field_id].ui_search_index_type_ == 0);
    EXPECT_TRUE(o_app_field_cnt.o_app_field_table_[i_appid][i_field_id].s_metric_type_ == "L2");

    int i_size = o_app_field_cnt.o_app_field_table_[i_appid][i_field_id].index_type_str_array_.size();
    EXPECT_TRUE(i_size == 3);

    std::string s_temp = o_app_field_cnt.o_app_field_table_[i_appid][i_field_id].index_type_str_array_[0];
    EXPECT_TRUE(s_temp == "IVF128,PQ16");
    
    s_temp = o_app_field_cnt.o_app_field_table_[i_appid][i_field_id].index_type_str_array_[1];
    EXPECT_TRUE(s_temp == "IDMap,HNSW32,Flat");

    s_temp = o_app_field_cnt.o_app_field_table_[i_appid][i_field_id].index_type_str_array_[2];
    EXPECT_TRUE(s_temp == "IVF128,Flat");

    i_size = o_app_field_cnt.o_app_field_table_[i_appid][i_field_id].index_dir_array_.size();
    EXPECT_TRUE(i_size == 3);

    s_temp = o_app_field_cnt.o_app_field_table_[i_appid][i_field_id].index_dir_array_[0];
    EXPECT_TRUE(s_temp == "/tmp/trained_index.faissindex");

    s_temp = o_app_field_cnt.o_app_field_table_[i_appid][i_field_id].index_dir_array_[1];
    EXPECT_TRUE(s_temp == "");

    s_temp = o_app_field_cnt.o_app_field_table_[i_appid][i_field_id].index_dir_array_[2];
    EXPECT_TRUE(s_temp == "/tmp/ivfflat.faissindex");

    i_field_id = 13;
    EXPECT_TRUE(o_app_field_cnt.o_app_field_table_[i_appid][i_field_id].ui_dim_ == 0);
};

TEST_F(ConfigParserTest, VetIdxCntTest){
    EXPECT_TRUE(o_vector_idx_cnt.s_service_name_ == "vector_index");
    EXPECT_TRUE(o_vector_idx_cnt.s_pid_file_ == "vector_index.pid");
    EXPECT_TRUE(o_vector_idx_cnt.s_log_dir_ == "../log/");
    EXPECT_TRUE(o_vector_idx_cnt.ui_log_level_ == 7);
    EXPECT_TRUE(o_vector_idx_cnt.b_daemon_);
    EXPECT_TRUE(o_vector_idx_cnt.s_listen_addr_ == "127.0.0.1");
    EXPECT_TRUE(o_vector_idx_cnt.ui_listen_port_ == 12004);
    EXPECT_TRUE(o_vector_idx_cnt.s_local_socket_ == "/tmp/vector_index");
    EXPECT_TRUE(o_vector_idx_cnt.s_rocksdb_dir_ == "../rocksdb");
};

TEST_F(ConfigParserTest, ConfigManagerTest){
    p_config_manager = vectorindex::ConfigManager::Instance();
    bool b_ret = p_config_manager->InitConfig();

    EXPECT_TRUE(b_ret);

    vectorindex::AppFieldContext::AppFieldTable* p_app_field_tab = 
     const_cast<vectorindex::AppFieldContext::AppFieldTable*>((&p_config_manager->GetAppFieldTable()->o_app_field_table_));

    EXPECT_TRUE(p_app_field_tab != NULL);

    EXPECT_TRUE(p_app_field_tab->size() == 1);

    int i_appid = 10008;
    int i_field_id = 14;

    EXPECT_TRUE(o_app_field_cnt.o_app_field_table_[i_appid][i_field_id].ui_dim_
            == (*p_app_field_tab)[i_appid][i_field_id].ui_dim_);
    
    EXPECT_TRUE(o_app_field_cnt.o_app_field_table_[i_appid][i_field_id].index_type_str_array_
            == (*p_app_field_tab)[i_appid][i_field_id].index_type_str_array_);

    EXPECT_TRUE(o_app_field_cnt.o_app_field_table_[i_appid][i_field_id].index_dir_array_
            == (*p_app_field_tab)[i_appid][i_field_id].index_dir_array_);
    
    EXPECT_TRUE(o_app_field_cnt.o_app_field_table_[i_appid][i_field_id].ui_search_index_type_
            == (*p_app_field_tab)[i_appid][i_field_id].ui_search_index_type_);

    EXPECT_TRUE(o_app_field_cnt.o_app_field_table_[i_appid][i_field_id].s_metric_type_
            == (*p_app_field_tab)[i_appid][i_field_id].s_metric_type_);

    vectorindex::VectorIndexContext* p_vet_idx = 
        const_cast<vectorindex::VectorIndexContext*>(p_config_manager->GetVectorIndexTable());
    
    EXPECT_TRUE(o_vector_idx_cnt.s_service_name_ == p_vet_idx->s_service_name_);
    EXPECT_TRUE(o_vector_idx_cnt.s_pid_file_ == p_vet_idx->s_pid_file_);
    EXPECT_TRUE(o_vector_idx_cnt.s_log_dir_ == p_vet_idx->s_log_dir_);
    EXPECT_TRUE(o_vector_idx_cnt.ui_log_level_ == p_vet_idx->ui_log_level_);
    EXPECT_TRUE(o_vector_idx_cnt.b_daemon_ == p_vet_idx->b_daemon_);
    EXPECT_TRUE(o_vector_idx_cnt.s_listen_addr_ == p_vet_idx->s_listen_addr_);
    EXPECT_TRUE(o_vector_idx_cnt.ui_listen_port_ == p_vet_idx->ui_listen_port_);
    EXPECT_TRUE(o_vector_idx_cnt.s_local_socket_ == p_vet_idx->s_local_socket_);
    EXPECT_TRUE(o_vector_idx_cnt.s_rocksdb_dir_ == p_vet_idx->s_rocksdb_dir_);
};

UNITEST_NAMESPACE_END
#endif