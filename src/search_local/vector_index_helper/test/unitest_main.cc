#include "vet_index_common_unitest.h"
#include "config_parser_unitest.h"
#include "faiss_index_factory_unitest.h"
#include "rocksdb_basic_operator_unitest.h"
#include "vet_cmd_handler_unitest.h"
#include "vet_protobuf_convertor_unitest.h"

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}