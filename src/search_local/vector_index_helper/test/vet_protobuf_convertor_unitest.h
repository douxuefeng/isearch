#ifndef VET_PROTOBUF_CONVERTOR_UNITEST_H_
#define VET_PROTOBUF_CONVERTOR_UNITEST_H_

#include "unitest_comm.h"
#include "../include/vet_protobuf_convertor.h"

UNITEST_NAMESPACE_BEGIN
TEST(ProtobufConvertTest , VectorReqTest) {
    VectorReq o_vet_req;
    o_vet_req.add_vector_data(0.001);
    o_vet_req.add_vector_data(0.002);
    o_vet_req.set_appid(10008);
    o_vet_req.set_field_id(14);
    o_vet_req.set_index_typeid(0);

    std::string s_parse_str;
    o_vet_req.SerializeToString(&s_parse_str);

    VetReqCnt o_req_cnt;
    bool b_ret = vectorindex::PbCvtor::ConvertPbStr2VetReqCnt(s_parse_str , o_req_cnt);
    EXPECT_TRUE(b_ret);

    EXPECT_TRUE(10008 == o_req_cnt.o_idx_flag_.i_appid_);
    EXPECT_TRUE(14 == o_req_cnt.o_idx_flag_.i_field_id_);
    EXPECT_TRUE(0 == o_req_cnt.o_idx_flag_.i_index_typeid_);

    for (int i = 0; i < (int)o_req_cnt.o_vet_data_.size(); i++)
    {
        EXPECT_TRUE(abs(0.001 - o_req_cnt.o_vet_data_[i]) < 0.0001
            || abs(0.002 - o_req_cnt.o_vet_data_[i]) < 0.0001);
    }
}

TEST(ProtobufConvertTest , VetRspTest) {
    VetRspCnt o_vet_rsp;
    o_vet_rsp.ui_code = 0;
    o_vet_rsp.s_info = "success";
    o_vet_rsp.vet_ids.emplace_back(1);
    o_vet_rsp.vet_ids.emplace_back(2);

    std::string s_parse_value;
    bool b_ret = vectorindex::PbCvtor::ConvertVetRspCnt2PbStr(o_vet_rsp , s_parse_value);
    EXPECT_TRUE(b_ret);

    VectorRsp o_rsp;
    b_ret = o_rsp.ParseFromString(s_parse_value);
    EXPECT_TRUE(b_ret);

    EXPECT_TRUE(0 == o_rsp.code());
    EXPECT_TRUE("success" == o_rsp.info());
    for (int i = 0; i < o_rsp.vector_id_size(); i++)
    {
        EXPECT_TRUE((1 == o_rsp.vector_id(i))
            || (2 == o_rsp.vector_id(i)));
    }
}

UNITEST_NAMESPACE_END
#endif
