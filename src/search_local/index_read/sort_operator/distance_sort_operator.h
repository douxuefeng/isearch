#ifndef DISTANCE_SORT_OPERATOR_H_
#define DISTANCE_SORT_OPERATOR_H_

#include "sort_operator_base.h"

class DistanceSortOperator : public SortOperatorBase
{
public:
    DistanceSortOperator(RequestContext* request_cnt , DocManager* doc_manager);
    virtual~ DistanceSortOperator();

private:
    virtual void RelevanceSort();
};
#endif