#ifndef QUERY_PROCESS_MOCK_TEST_H_
#define QUERY_PROCESS_MOCK_TEST_H_

#include "unittest_comm.h"
#include "../process/query_process.h"

UNITEST_NAMESPACE_BEGIN
class MockQueryProcess : public QueryProcess{
public:
    MockQueryProcess() : QueryProcess() {
        component_ = new RequestContext();
        doc_manager_ = new DocManager(component_);
        request_ = new CTaskRequest();
    };
    ~MockQueryProcess(){
        DELETE(component_);
        DELETE(doc_manager_);
        DELETE(request_);
    }
    MOCK_METHOD(int, ParseContent, (int));
    MOCK_METHOD(int, GetValidDoc, (int logic_type , const std::vector<FieldInfo>& keys));
    MOCK_METHOD(int, ParseContent, ());
    MOCK_METHOD(int, GetValidDoc, ());
    MOCK_METHOD(int, CheckValidDoc, ());
    MOCK_METHOD(int, GetScore, ());
    MOCK_METHOD(void, SortScore, (int& i_sequence , int& i_rank));
};

class QueryProcessTest : public testing::Test {
protected:
    QueryProcessTest():query_process_(&query_process_mock_){};

    QueryProcess* query_process_; 
    MockQueryProcess query_process_mock_;
};

TEST_F(QueryProcessTest , StartQueryTest){
    EXPECT_CALL(query_process_mock_ , ParseContent()).Times(AnyNumber())
        .WillOnce(Return(0)).WillOnce(Return(1))
        .WillRepeatedly(Return(0));

    EXPECT_CALL(query_process_mock_ , GetValidDoc()).Times(AnyNumber())
        .WillRepeatedly(Return(0));

    EXPECT_CALL(query_process_mock_ , CheckValidDoc()).Times(AnyNumber())
        .WillOnce(Return(1)).WillOnce(Return(0))
        .WillRepeatedly(Return(0));

    EXPECT_CALL(query_process_mock_ , GetScore()).Times(AnyNumber())
        .WillOnce(Return(1)).WillOnce(Return(0))
        .WillRepeatedly(Return(0));
    EXPECT_CALL(query_process_mock_ , SortScore(testing::_, testing::_)).Times(AnyNumber())
        .WillRepeatedly(Return());
    EXPECT_NE(0, query_process_->StartQuery());
    EXPECT_NE(0, query_process_->StartQuery());
    EXPECT_EQ(0, query_process_->StartQuery());
}
UNITEST_NAMESPACE_END
#endif