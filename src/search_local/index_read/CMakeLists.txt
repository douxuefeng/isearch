option(jdtestOpen "Build gtest's units programs." OFF)
option(INDEX_READ_OUTPUT "Build index_read programs." ON)
cmake_minimum_required(VERSION 2.6)

PROJECT(index_read)
EXECUTE_PROCESS(COMMAND git log -1 --pretty=format:%h . OUTPUT_VARIABLE version)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++0x -fprofile-arcs -ftest-coverage -g -O2 -lrt -Wall")

AUX_SOURCE_DIRECTORY(. main)
AUX_SOURCE_DIRECTORY(./index_sync index_sync)
AUX_SOURCE_DIRECTORY(./utils utils)
AUX_SOURCE_DIRECTORY(./correction correction)
AUX_SOURCE_DIRECTORY(./query query)
AUX_SOURCE_DIRECTORY(./process process)
AUX_SOURCE_DIRECTORY(./sort_operator sort_operator)
AUX_SOURCE_DIRECTORY(./query_operator query_operator)

LINK_DIRECTORIES(
${PROJECT_SOURCE_DIR}/.
${PROJECT_SOURCE_DIR}/../../comm
${PROJECT_SOURCE_DIR}/../../3rdlib/jsoncpp/lib
${PROJECT_SOURCE_DIR}/../../3rdlib/google_test/lib
${PROJECT_SOURCE_DIR}/../../3rdlib/attr_api
${PROJECT_SOURCE_DIR}/../../comm/stat
${PROJECT_SOURCE_DIR}/../index_storage/api/c_api_cc
)

ADD_EXECUTABLE(index_read ${main} ${index_sync} ${utils} ${correction} ${query} ${process} ${sort_operator} ${query_operator})

target_include_directories(index_read  PUBLIC
../../3rdlib
../../3rdlib/jsoncpp/include
../../comm
../../comm/stat
../../comm/segment
../../comm/protoc
../index_storage/api/c_api_cc
)

add_definitions(-DACTIVE_GIT_VERSION="${version}" -DMAIN)

target_link_libraries(index_read  libcommon.a libdtc.so jsoncpp stat ssl pthread dl protobuf protoc)
SET_TARGET_PROPERTIES(index_read  PROPERTIES RUNTIME_OUTPUT_DIRECTORY "./bin")


if(jdtestOpen)
    cmake_minimum_required(VERSION 2.8.8)

    if (POLICY CMP0048)
        cmake_policy(SET CMP0048 NEW)
    endif (POLICY CMP0048)
    
    if (CMAKE_VERSION VERSION_LESS "3.1")
        add_definitions(-std=c++11)
    else()
        set(CMAKE_CXX_STANDARD 11)
        set(CMAKE_CXX_STANDARD_REQUIRED ON)
        if(NOT CYGWIN)
            set(CMAKE_CXX_EXTENSIONS OFF)
        endif()
    endif()

    list(REMOVE_ITEM main "./index_read.cc")
    AUX_SOURCE_DIRECTORY(./unittest jdtestFiles)

    ADD_EXECUTABLE(gtest_index_read ${main} ${jdtestFiles} ${index_sync} ${utils} ${correction} ${query} ${process} ${sort_operator})
    target_include_directories(gtest_index_read  PUBLIC
    ../../3rdlib
    ../../3rdlib/jsoncpp/include
    ../../3rdlib/google_test/include
    ../../comm
    ../../comm/stat
    ../../comm/segment
    ../../comm/protoc
    ../index_storage/api/c_api_cc
    ./correction
    )
    target_link_libraries(gtest_index_read common dtc gtest_main gmock gtest jsoncpp stat dl pthread m protobuf protoc)
    SET_TARGET_PROPERTIES(gtest_index_read  PROPERTIES RUNTIME_OUTPUT_DIRECTORY "./bin")
    install(TARGETS gtest_index_read RUNTIME DESTINATION bin)
endif()


