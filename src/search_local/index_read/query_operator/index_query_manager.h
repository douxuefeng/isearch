/*
 * =====================================================================================
 *
 *       Filename:  index_tbl_op.h
 *
 *    Description:  index tbl op class definition.
 *
 *        Version:  1.0
 *        Created:  19/03/2018
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  zhulin, shzhulin3@jd.com
 *                  chenyujie, chenyujie28@jd.com
 *        Company:  JD.com, Inc.
 *
 * =====================================================================================
 */
#ifndef INDEX_QUERY_MANAGER_H_
#define INDEX_QUERY_MANAGER_H_

#include "singleton.h"
#include "noncopyable.h"

#include "hanpin_index_query.h"
#include "invert_index_query.h"

enum INDEX_QUERY_TYPE
{
    E_INDEX_QUERY_DEF,
    E_INDEX_QUERY_HANPIN,
    E_INDEX_QUERY_INVERT,
    E_INDEX_QUERY_NUM
};

class IndexQueryManager : private noncopyable
{
public:
    IndexQueryManager() 
        : p_current_index_query_(NULL)
        , index_query_map_()
        {
            index_query_map_.insert(std::move(
                std::make_pair(E_INDEX_QUERY_DEF , new DefaultIndex())
            ));
            index_query_map_.insert(std::move(
                std::make_pair(E_INDEX_QUERY_HANPIN , new HanpinIndex())
            ));
            index_query_map_.insert(std::move(
                std::make_pair(E_INDEX_QUERY_INVERT , new InvertIndex())
            ));
        };
    virtual ~IndexQueryManager() {
        std::map<uint32_t , IndexTableBase*>::iterator iter = index_query_map_.begin();
        for (; iter != index_query_map_.end(); ++iter) {
            DELETE(iter->second);
        }
        index_query_map_.clear();
    };

public:
    static IndexQueryManager* Instance(){
        return CSingleton<IndexQueryManager>::Instance();
    };

    static void Destroy(){
        CSingleton<IndexQueryManager>::Destroy();
    };

public:
    IndexTableBase* const GetInxQueIns(uint32_t i_type) {
        if (i_type >= E_INDEX_QUERY_NUM ||
            i_type <= E_INDEX_QUERY_DEF) {
            return index_query_map_[E_INDEX_QUERY_DEF];
        }
        p_current_index_query_ = index_query_map_[i_type];
        return p_current_index_query_;
    };

    void SetCurIndexQuery(IndexTableBase* p_index_query){
        p_current_index_query_ = p_index_query;
    };

private:
    IndexTableBase* p_current_index_query_;
    std::map<uint32_t , IndexTableBase*> index_query_map_;
};
#endif