/*
 * =====================================================================================
 *
 *       Filename:  index_tbl_op.h
 *
 *    Description:  index tbl op class definition.
 *
 *        Version:  1.0
 *        Created:  19/03/2018
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  zhulin, shzhulin3@jd.com
 *                  chenyujie, chenyujie28@jd.com
 *        Company:  JD.com, Inc.
 *
 * =====================================================================================
 */
#ifndef INVERT_INDEX_QUERY_H_
#define INVERT_INDEX_QUERY_H_

#include "index_query_base.h"

class InvertIndex : public IndexTableBase{
public:
	InvertIndex();
	virtual~ InvertIndex();

public:
	virtual int InitServer(const SDTCHost& dtchost, const std::string& bindAddr = "127.0.0.1");

public:
	virtual bool DocValid(uint32_t appid, string doc_id, bool &is_valid);
	virtual bool DocValid(uint32_t appid, const std::vector<IndexInfo>& vecs, bool need_version, std::map<std::string, uint32_t>& valid_version, hash_string_map& doc_content_map);
	virtual bool TopDocValid(uint32_t appid, std::vector<TopDocInfo>& no_filter_docs, std::vector<TopDocInfo>& doc_info);

	virtual bool GetDocInfo(uint32_t appid, const std::string& word, uint32_t field_id, std::vector<IndexInfo>& doc_info);
	virtual bool GetTopDocInfo(uint32_t appid, std::string word, std::vector<TopDocInfo>& doc_info);

	virtual bool GetScoreByField(uint32_t appid, std::string doc_id, std::string sort_field, uint32_t sort_type, ScoreInfo &score_info);
	virtual bool GetDocContent(uint32_t appid, const std::vector<IndexInfo>& index_infos, hash_string_map& doc_content);
	
	virtual int GetDocCnt(uint32_t appid);
	virtual bool GetContentByField(uint32_t appid, std::string doc_id, uint32_t doc_version, const std::vector<std::string>& fields, Json::Value &value);

private:
	bool getSnapshotContent(int left, int right, uint32_t appid , const std::vector<IndexInfo>& index_infos, hash_string_map& doc_content);
	bool getSnapshotExecute(int left, int right, uint32_t appid, const std::vector<IndexInfo>& no_filter_docs, std::vector<DocVersionInfo>& docVersionInfo);
	bool getTopSnapshotExecute(int left, int right, uint32_t appid, std::vector<TopDocInfo>& no_filter_docs, std::vector<DocVersionInfo>& docVersionInfo);
};

#endif