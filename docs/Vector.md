## 背景

向量检索是指用一组数字(向量)来量化一个事物，用大量向量来表示事物集合，用向量计算的方式寻找相似事物的一种检索方式。

isearch底层采用的向量检索框架为Facebook AI的Faiss，项目地址为：https://github.com/facebookresearch/faiss 

## app_field_define表

在app_field_define表定义时，vector字段类型需定义好dim、index_type和metric_type三个属性，示例如下：
```
{
     "id":3,
     "appId":10065,
     "fieldName":"float_vector",
     "fieldType":15,
     "fieldId":3,
     "IsPrimaryKey":0,
     "indexTag":0,
     "snapshotTag":1,
     "segmentTag":0,
     "segmentFeature":0,
     "unionField":"",
     "createTime":"2021/4/13 15:49:09",
     "dim":128, // 维数
     "index_type": [" PCA80,Flat "], // 索引类型，格式与faiss对外工厂类设置保持一致
     "metric_type": "L2" // 距离计算方式，可选值：InnerProduct、L2
}

```

说明：index_type参考https://github.com/facebookresearch/faiss/wiki/The-index-factory

## 向量插入示例

```
curl -X POST \
  http://127.0.0.1/insert \
  -H 'content-type: application/json' \
  -H 'doc_id: 1' \
  -d '{"appid":10065,"table_content":{"cmd":"add","fields":{"doc_id":"1","random_value":1488981884,"float_vector":[0.005653954876242762, 0.632130963117687, 0.7519577013172226, 0.8568273368123129, 0.2034335192251041, 0.9786219451736441, 0.5948105950093241, 0.9618089054657426]}}}'
```

## 向量查询示例

```
curl -X POST \
  http://127.0.0.1/search \
  -H 'content-type: application/json' \
  -d '{"appid":10065,"query":{"vector_query":{"float_vector":[0.005653954876242762, 0.632130963117687, 0.7519577013172226, 0.8568273368123129, 0.2034335192251041, 0.9786219451736441, 0.5948105950093241, 0.9618089054657426], "index_type_id":1}} }'
```